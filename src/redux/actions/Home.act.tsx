// import * as types from '../constants/home.const';

// export const LogOut = () => {
//   return {
//     type: types.LOGOUT,
//   };
// };
import AsyncStorage from '@react-native-community/async-storage'

export const HomeAction = () => {

  return (dispatch: (arg0: { type: string; payload?: any }) => void, getState: any) => {

    dispatch({ type: 'LOGOUT' })
    AsyncStorage.removeItem('status');
    
  }

}