import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import { Alert } from 'react-native'

export const LoginAction = (username: string, password: string) => {

  return (dispatch: (arg0: { type: string; payload?: any }) => void, getState: any) => {

    dispatch({ type: 'LOGIN_REQUEST' })
    axios.request(
      {
        url: 'https://moki.com.vn/api/login',
        method: 'POST',
        data: {
          email_or_phone: username,
          password: password,
        },
      }).then(function (response) {
        console.log(response.data.status)
        dispatch({ type: 'LOGIN_SUCCESS', payload: response.data.status })
        if (response.data.status == 'success') {
          AsyncStorage.setItem('status', response.data.status);
        }
        else {
          Alert.alert('Login failed! Please checked your email and password!')
        }
      }).catch(function (error) {
        dispatch({ type: 'LOGIN_FAIL', payload: error })
      })
  }

}