import { createStore, applyMiddleware } from 'redux'
import LoginReducer from '../reducer/Login_reducer'
import thunk from 'redux-thunk'

const store = createStore(LoginReducer, applyMiddleware(thunk))

export default store