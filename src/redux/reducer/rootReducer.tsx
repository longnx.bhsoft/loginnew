import {combineReducers} from 'redux';

import LoginReducer from './Login_reducer';
import Home_red from './Home_reducer';

export default combineReducers({
  Login: LoginReducer,
  Home: Home_red,
});
