
let initialState = {
  data: '',
  isLoading: false,
  error: null
} 

export default LoginReducer = (state = initialState, action: { type: any; payload: any }) => {
  switch (action.type) {
      case 'LOGIN_REQUEST':
          return Object.assign({}, state, { isLoading: true })
      case 'LOGIN_SUCCESS':
          return Object.assign({}, state, { data: action.payload, isLoading: false })
      case 'LOGIN_FAIL':
          return Object.assign({}, state, { error: action.payload, isLoading: false })
      default:
          return state
  }
}