import React from 'react';
import { View, Text, OpaqueColorValue, TextInput } from 'react-native'

import images from '../../assets/images'
import styles from './style';

interface Props {
    isPassword?: boolean,
    placeHolder?: string,
    placeholderTextColor?: OpaqueColorValue,
    title_Input?: string,
    keyboardType?: "default" | "email-address" | "numeric" | "phone-pad" | "number-pad" | "decimal-pad" | "visible-password" | "ascii-capable" | "numbers-and-punctuation" | "url" | "name-phone-pad" | "twitter" | "web-search" | undefined,
    surcureTextEntry?: boolean,
    Multiline?: boolean,
    onChangeText?: ((text: string) => void) | undefined
}

const Inputs = (props: Props) => {
    return (
        <View style={styles.container}>
            <Text style={styles.title_Email}>{props.title_Input}</Text>
            <TextInput
                style={styles.text_Input_style}
                placeholder={props.placeHolder}
                placeholderTextColor={props.placeholderTextColor}
                keyboardType={props.keyboardType}
                secureTextEntry={props.surcureTextEntry}
                multiline={props.Multiline}
                onChangeText={props.onChangeText}
            /> 

        </View>
    )
}
export default Inputs;
