import React from 'react';
import { TouchableOpacity, Text, Image, ImageSourcePropType } from 'react-native';

import styles from './style'
interface Props {
     style?: Styles,
     title: string,
     onPress?: () => void,
     image?: ImageSourcePropType,
     HaveImage: Boolean
}

interface Styles {
     containerStyle?: Object,
     titleStyle?: Object
}
const ButtonSuccess = (props: Props) => {

     return (
          <TouchableOpacity style={[styles.container, props.style?.containerStyle]} onPress={props.onPress}>
               {
                    props.HaveImage ? <Image source={props.image} style={{ alignSelf: 'center', marginRight: 10 }} /> : null
               }
               <Text style={[styles.title_button, props.style?.titleStyle]}>{props.title}</Text>
          </TouchableOpacity>
     );
};

export default ButtonSuccess;