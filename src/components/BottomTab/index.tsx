import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

//components
import ItemTabBarBottom from './ItemTabNavigation'

//icon
import images from '../../assets/images'

//theme
import { Palette } from '../../themes/colors'

//screen
import Inputs from '../../containers/InputsScreen';
import Database from '../../containers/ShowDataScreen'
import Setting from '../../containers/SettingScreen'


const Tab = createBottomTabNavigator()

const customeTabOptions = {
  activeTintColor: Palette.red,
  style: {
    height: 68
  },
  labelStyle: {
    bottom: 10
  },
  showLabel: false
}




enum ScreenName {
  Inputs = "Inputs",
  Database = "Database",
  Setting = "Setting",
}

const BottomTab = () => {
  return (
    <Tab.Navigator
      screenOptions={({ route, navigation }) => {
        return {
          tabBarIcon: ({ focused, color, size }) => {
            let image;
            switch (route.name) {
              case ScreenName.Inputs:
                image = images.icon_Input;
                break;
              case ScreenName.Database:
                image = images.icon_Database;
                break;
              case ScreenName.Setting:
                image = images.icon_setting;
                break;
            }

            return (<ItemTabBarBottom
              imageUri={image}
              tintColor={color}
              isFocused={focused}
            />)
          }
        }
      }}
      tabBarOptions={
        customeTabOptions
      }
    >
      <Tab.Screen name={ScreenName.Inputs} component={Inputs} />
      <Tab.Screen name={ScreenName.Database} component={Database} />
      <Tab.Screen name={ScreenName.Setting} component={Setting} />

    </Tab.Navigator>
  )
}

export default BottomTab;