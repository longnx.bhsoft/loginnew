import React from "react";
import { StyleSheet, Dimensions } from "react-native";
import { Palette } from '../../themes/colors';
const Widths = Dimensions.get('window').width;
const Heights = Dimensions.get('window').height;
const styles = StyleSheet.create({
    container: {
        width: Widths / 12,
        height: Widths / 12,
        justifyContent:'center',
        alignSelf:'center'
    },
    arrow_back_image:{
        alignSelf:'center'
    }

})
export default styles;
