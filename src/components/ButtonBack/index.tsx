import React from 'react';
import { View, Text, Image, TouchableOpacity} from 'react-native'

import images from '../../assets/images'
import styles from './style';

interface Props {
    onPress: ()=> void
}

const ButtonBack = (props: Props) => {
    return (
        <TouchableOpacity style={styles.container} onPress={props.onPress}>
            <Image source={images.arrow_back}  style={styles.arrow_back_image}/>
        </TouchableOpacity>
    )
}
export default ButtonBack;
