export const Palette = {
    black:      '#000000',
    white:      '#ffffff',
    red:        '#e00051',
    grey:       '#697684',
    blue:       '#71b6f9',
};