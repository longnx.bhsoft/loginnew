import 'react-native-gesture-handler';
import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { useSelector } from 'react-redux';

import LoginScreen from '../containers/LoginScreen';
import SettingScreen from '../containers/SettingScreen';
import AsyncStorage from '@react-native-community/async-storage';
import ButtonTab from '../components/BottomTab'

const Navigator = () => {
  const [login, setLogin] = useState('Login FAIL')
  const Stack = createStackNavigator();

  useEffect(() => {
    AsyncStorage.getItem('status').then(
      (data) => setLogin(data),
    );
  })
  const status = true ? login == 'success' : false
  
  console.log(status)
  return (
    <NavigationContainer>
      {status ? (
        <Stack.Navigator>
          <Stack.Screen
            name="Home"
            options={{ headerShown: false }}
            component={ButtonTab}
          />
          <Stack.Screen
            name="Setting"
            options={{ headerShown: false }}
            component={SettingScreen}
          />
        </Stack.Navigator>
      ) : (
          <Stack.Navigator>
            <Stack.Screen
              options={{ headerShown: false }}
              name="Login"
              component={LoginScreen}
            />
          </Stack.Navigator>
        )}

    </NavigationContainer>
  );
};
export default Navigator;
