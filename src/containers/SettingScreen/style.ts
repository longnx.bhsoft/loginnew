import React from "react";
import { StyleSheet, Dimensions } from "react-native";
import { Palette } from '../../themes/colors';
const Widths = Dimensions.get('window').width;
const Heights = Dimensions.get('window').height;
const styles = StyleSheet.create({

    container: {
        flex: 1,
    },
    profilio:{
        width: Widths - Widths * 0.1,
        height: Heights/6,
        borderRadius: 10,
        alignSelf: 'center',
        marginTop: 20,
        backgroundColor:Palette.blue,
        elevation: 10,
        shadowOffset: {
            width: 8,
            height: 8,
        },
        flexDirection: 'row'
    },    
    About:{
        width: Widths - Widths * 0.1,
        height: Heights / 3,
        marginTop: 20,
        backgroundColor: Palette.white,
        borderRadius: 5,
        justifyContent: 'center',
        alignSelf: 'center',
        elevation: 10,
        shadowOffset: {
            width: 8,
            height: 8,
        },
    },
    Profile: {
        width: Widths / 5,
        height: Widths / 5,
        borderRadius: 20,
        marginLeft: 15,
        alignSelf: 'center',
    },
    Text_Profile:{
        alignSelf: 'center',
        marginLeft:20
    },
    Font:{
        fontSize: 20,
        color: Palette.white
    },
    img_profile:{
        width: Widths/5,
        height: Widths / 5,
    },
    feel:{
        color: Palette.black
    },
    Tough__help_feedback:{
        width: "100%",
        height: "100%",
        flexDirection: 'row'
    },
    help_feedback: {
        width: "85%",
        height: "30%",
        borderBottomWidth: 1.2,
        borderColor: "#B7B7B7",
        alignSelf: 'center',
        
    },
    icon_help_feedback: {
        justifyContent: 'center',
    },
    title_help_feedback: {
        justifyContent: 'center',
        marginLeft: Widths / 15,
    },
    Text_title_help_feedback :{
        fontSize: 18
    },
    Img_help_feedback:{
        width: Widths/12, 
        height: Widths / 12,
        alignSelf: 'center'
    },
    AboutUs: {
        width: "85%",
        height: "30%",
        borderBottomWidth: 1.2,
        borderColor: "#B7B7B7",
        alignSelf: 'center'
    },
    Tough__AboutUs: {
        width: "100%",
        height: "100%",
        flexDirection: 'row',
        justifyContent:'space-between',
    },
    About_view:{
        width: "80%",
        height: "100%",
        flexDirection: 'row',
        
    },
    Text_AboutUs:{
        alignSelf: 'center',
        color: "#B7B7B7",
    },
    Logout: {
        width: "85%",
        height: "30%",
        borderColor: "#B7B7B7",
        alignSelf: 'center'
    },
    Text_title_logout:{
        color: Palette.red,
        fontSize: 18
    },
    
    
    
})
export default styles