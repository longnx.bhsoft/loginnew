import React from 'react';
import { View, Text, Image, TouchableOpacity, SafeAreaView, Alert } from 'react-native'
import { useDispatch } from 'react-redux';
import images from '../../assets/images';

//Components
import { LogOut } from '../../redux/actions/Home.act';
import styles from './style';

const SettingScreen = () => {
    const dispatch = useDispatch();

    const logOut = () => {
        dispatch(LogOut());
    };
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.profilio}>
                <View style={styles.Profile} >
                    <Image source={images.Profile} style={styles.img_profile} />
                </View>
                <View style={styles.Text_Profile}>
                    <Text style={styles.Font}>Nguyễn Xuân Long</Text>
                    <Text style={styles.feel}>Thời tiết hôm nay thật đẹp!</Text>
                </View>
            </View>
            <View style={styles.About}>
                <View style={styles.help_feedback}>
                    <TouchableOpacity style={styles.Tough__help_feedback} onPress={() => {
                        Alert.alert('Liên hệ', 'Mọi thắc mắc & phản hồi vui lòng gửi mail về longnx.bhsoft@gmail.com. Xin trân trọng cảm ơn!')
                    }}>
                        <View style={styles.icon_help_feedback}>
                            <Image source={images.Feedback} style={styles.Img_help_feedback} />
                        </View>
                        <View style={styles.title_help_feedback}>
                            <Text style={styles.Text_title_help_feedback}>Help & Feedback</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.AboutUs}>
                    <TouchableOpacity style={styles.Tough__AboutUs}>
                        <View style={styles.About_view} >
                            <View style={styles.icon_help_feedback}>
                                <Image source={images.Version} style={styles.Img_help_feedback} />
                            </View>
                            <View style={styles.title_help_feedback}>
                                <Text style={styles.Text_title_help_feedback}>About app</Text>
                            </View>
                        </View>
                        <Text style={styles.Text_AboutUs}>v1.0</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.Logout}>
                    <TouchableOpacity style={styles.Tough__help_feedback} onPress={() => {
                        Alert.alert(
                            'Đăng xuất',
                            'Bạn có chắc chắn rằng bạn muốn đăng xuất?',
                            [
                                {
                                    text: 'Thoát',
                                    onPress: () => {
                                        return null;
                                    },
                                },
                                {
                                    text: 'Đồng ý',
                                    onPress: logOut
                                },
                            ],
                            { cancelable: false },
                        );
                    }}>
                        <View style={styles.icon_help_feedback}>
                            <Image source={images.Logout} style={styles.Img_help_feedback} />
                        </View>
                        <View style={styles.title_help_feedback}>
                            <Text style={styles.Text_title_logout}>LOG OUT</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </SafeAreaView>
    )
}
export default SettingScreen;
