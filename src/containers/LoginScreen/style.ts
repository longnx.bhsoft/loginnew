import React from "react";
import { StyleSheet, Dimensions } from "react-native";
import { Palette } from '../../themes/colors';
const Widths = Dimensions.get('window').width;
const Heights = Dimensions.get('window').height;
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    Button_Top_View: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '90%',
        height: Heights / 13,
        alignSelf: 'center',
    },
    Button_Forgot_PassWord: {
        justifyContent: 'center'
    },
    text_Button_Forgot: {
        fontSize: 17,
        color: Palette.grey
    },
    Title_Top_View: {
        flexDirection: 'row',
        width: '90%',
        height: Heights / 13,
        alignSelf: 'center',
        marginTop: Heights/22,
        justifyContent:'center',
    },
    title_Login:{
        fontSize: 32,
        fontWeight:'bold',
    },
    inputs:{
        width: '90%',
        height: Heights / 4.5,
        justifyContent:'space-between',
        alignSelf:'center'
    },
    button_Success:{
        width: '90%',
        height: Heights/12,
        backgroundColor: Palette.blue,
        marginTop: Heights/19 
    },
    title_style:{
        color: Palette.white,
        fontSize: 17,
        fontWeight: 'bold'
    }

})
export default styles;
