import React, { useState, useCallback, useEffect } from 'react';
import { View, Text, Image, TouchableOpacity, SafeAreaView} from 'react-native'

//Components
import ButtonSuccess from '../../components/ButtonSuccess';
import Inputs from '../../components/Inputs'
import styles from './style'; 
import * as LoginAction from '../../redux/actions/Login.act'
import { connect } from 'react-redux'

//redux-sagas
import { useDispatch } from "react-redux";
import Loader from '../../components/Loader'
const LoginScreen = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(false);
    useEffect(() => {
        return () => {
            if((state: any) => state.Home.loading == true){
                setLoading(true)
            }
            else{
                setLoading(false)
            }
        }
    })
    const Submit = () => {
        dispatch(LoginAction.LoginAction(username, password))
    };
  
    const handleChangeUserName = useCallback((text: any) => {
      setUsername(text);
    }, []);
  
    const handleChangePassword = useCallback((text: any) => {
      setPassword(text);
    }, []);
    return (
        <SafeAreaView style={styles.container}>
            
            <View style={styles.Button_Top_View}>
                <View></View>
                <TouchableOpacity style={styles.Button_Forgot_PassWord}>
                    <Text style={styles.text_Button_Forgot} >Forgot Password?</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.Title_Top_View}>
                <Text style={styles.title_Login}>LOGIN</Text>
            </View>
            <View style={styles.inputs}>
                <Inputs
                    title_Input={"Email"}
                    keyboardType={'email-address'}
                    placeHolder={'longnx.bhsoft@gmail.com'}
                    surcureTextEntry={false}
                    onChangeText={handleChangeUserName}
                />
                <Inputs
                    title_Input={"Password"}
                    surcureTextEntry={true}
                    keyboardType={'default'}
                    placeHolder={'***********'}
                    onChangeText={handleChangePassword}
                />

            </View>
            <ButtonSuccess
                title={'Login'}
                style={{
                    containerStyle: styles.button_Success,
                    titleStyle: styles.title_style,
                }}
                HaveImage={false}
                onPress={Submit}
            />

        </SafeAreaView>
    )
}
export default LoginScreen;
